
#include "resampler.hpp"

Resampler::Resampler(float rate, unsigned int m, float fc, float As,
                     unsigned int npfb) {
  q = resamp_rrrf_create(rate, m, fc, As, npfb);
}

Resampler::Resampler(float rate) { q = resamp_rrrf_create_default(rate); }

Resampler::~Resampler() { resamp_rrrf_destroy(q); }

void Resampler::Print() { resamp_rrrf_print(q); }

void Resampler::Reset() { resamp_rrrf_reset(q); }

unsigned int Resampler::GetDelay() { return resamp_rrrf_get_delay(q); }

void Resampler::SetRate(float rate) { resamp_rrrf_set_rate(q, rate); }

void Resampler::AdjustRate(float delta) { resamp_rrrf_adjust_rate(q, delta); }

void Resampler::Execute(float &x, std::vector<float> &y) {
  unsigned int yn;
  resamp_rrrf_execute(q, x, y.data(), &yn);
  y.resize(yn);
}

void Resampler::Execute(std::vector<float> &x, std::vector<float> &y) {
  unsigned int yn;
  resamp_rrrf_execute_block(q, x.data(), (unsigned int)x.size(), y.data(), &yn);
  y.resize(yn);
}
