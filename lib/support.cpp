#include <complex>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "support.hpp"

void PlotSignals(std::string file, const std::vector<float> &x,
                 const std::vector<std::complex<float>> &y,
                 const std::vector<float> &z, float f_sample) {
  std::ofstream f;
  f.open(file);

  f << std::right << std::fixed << std::setprecision(8);

  f << "# " << std::setw(6) << "index"
    << " " << std::setw(12) << "time"
    << " " << std::setw(12) << "real(x)"
    << " " << std::setw(12) << "real(y)"
    << " " << std::setw(12) << "imag(y)"
    << " " << std::setw(12) << "real(z)" << std::endl;

  for (unsigned int i = 0; i < x.size(); i++) {
    f << std::setw(12) << i << " " << std::setw(12) << (float)i / f_sample
      << " " << std::setw(12) << x[i] << " " << std::setw(12) << y[i].real()
      << " " << std::setw(12) << y[i].imag() << " " << std::setw(12) << z[i]
      << std::endl;
  }

  f.close();
}

void PlotSignal(std::string file, const std::vector<float> &x, float f_sample) {
  std::ofstream f;
  f.open(file);

  f << std::right << std::fixed << std::setprecision(8);

  f << "# " << std::setw(6) << "index"
    << " " << std::setw(12) << "time"
    << " " << std::setw(12) << "value" << std::endl;

  for (unsigned int i = 0; i < x.size(); i++) {
    f << std::setw(12) << i << " " << std::setw(12) << (float)i / f_sample
      << " " << std::setw(12) << x[i] << std::endl;
  }

  f.close();
}

void PlotSignal(std::string file, const std::vector<std::complex<float>> &x,
                float f_sample) {
  std::ofstream f;
  f.open(file);

  f << std::right << std::fixed << std::setprecision(8);

  f << "# " << std::setw(6) << "index"
    << " " << std::setw(12) << "time"
    << " " << std::setw(12) << "real"
    << " " << std::setw(12) << "imag" << std::endl;

  for (unsigned int i = 0; i < x.size(); i++) {
    f << std::setw(12) << i << " " << std::setw(12) << (float)i / f_sample
      << " " << std::setw(12) << x[i].real() << " " << std::setw(12)
      << x[i].imag() << std::endl;
  }

  f.close();
}

void AddSignals(std::vector<float> &a, std::vector<float> &b,
                std::vector<float> &c) {
  for (unsigned int i = 0; i < a.size(); i++) {
    c[i] = a[i] + b[i];
  }
}

void AddSignals(std::vector<float> &a, std::vector<float> &b) {
  AddSignals(a, b, a);
}

void AddSignals(std::vector<std::complex<float>> &a,
                std::vector<std::complex<float>> &b,
                std::vector<std::complex<float>> &c) {
  for (unsigned int i = 0; i < a.size(); i++) {
    c[i] = a[i] + b[i];
  }
}

void AddSignals(std::vector<std::complex<float>> &a,
                std::vector<std::complex<float>> &b) {
  AddSignals(a, b, a);
}

void GenerateSineWave(std::vector<float> &s, float sr, float f, float a) {
  double omega = 2 * M_PI * f / ((double)sr);

  for (unsigned int i = 0; i < s.size(); i++) {
    s[i] = a * (float)sin(omega * (double)i);
  }
}
