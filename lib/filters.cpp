#include "filters.hpp"

IIRFilter::IIRFilter(float f_carrier, float f_width, float f_sample) {
  float f0 = f_carrier / f_sample;
  float fc = (f_carrier - f_width / 2) / f_sample;

  q = iirfilt_crcf_create_prototype(LIQUID_IIRDES_ELLIP, LIQUID_IIRDES_BANDPASS,
                                    LIQUID_IIRDES_SOS, FILTER_ORDER, fc, f0,
                                    FILTER_RIPPLE, FILTER_STOP);
}

IIRFilter::~IIRFilter() { iirfilt_crcf_destroy(q); }

void IIRFilter::Print() { iirfilt_crcf_print(q); }

void IIRFilter::Reset() { iirfilt_crcf_reset(q); }

void IIRFilter::Execute(std::complex<float> &x, std::complex<float> *y) {
  iirfilt_crcf_execute(q, (liquid_float_complex)x, (liquid_float_complex *)y);
}

void IIRFilter::Execute(std::vector<std::complex<float>> &x,
                        std::vector<std::complex<float>> &y) {
  iirfilt_crcf_execute_block(q, (liquid_float_complex *)x.data(),
                             (unsigned int)x.size(),
                             (liquid_float_complex *)y.data());
}

unsigned int IIRFilter::GetLength() { return iirfilt_crcf_get_length(q); }

void IIRFilter::FrequencyResponse(float fc, std::complex<float> *H) {
  iirfilt_crcf_freqresponse(q, fc, (liquid_float_complex *)H);
}

float IIRFilter::GroupDelay(float fc) { return iirfilt_crcf_groupdelay(q, fc); }
