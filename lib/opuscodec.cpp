#include "opuscodec.hpp"

OpusCodec::OpusCodec(int frame_size, int sample_rate, int channels,
                      int bit_rate,  int max_frame_size, int max_packet_size,
                     int application) {
  s_frame_size = frame_size;
  s_sample_rate = sample_rate;
  s_channels = channels;
  s_max_frame_size = max_frame_size;
  s_max_packet_size = max_packet_size;
  s_application = application;

  p_bit_rate = bit_rate;
}

opuscodec_status_t OpusCodec::Encode(opus_int16 *input, int input_nb_samples) {
  if (verbose) {
    std::cout << "OpusCodec::Encode:: encode -> input_nb_samples: "
              << input_nb_samples << std::endl;
  }
  
  // Check if number of samples in input is equal to frame_size
  if (input_nb_samples != s_frame_size) {
    std::cout
        << "Failure::Encode:: input stream is not of size frame_size, size is: "
        << input_nb_samples << std::endl;
    return OPUSCODEC_FAILURE;
  }

  // Check if encoder exists
  if (p_encoder == nullptr) {
    if (verbose) {
      std::cout << "create encoder" << std::endl;
    }
    
    // encoder does not yet exit -> generate encoder
    p_encoder =
        opus_encoder_create(s_sample_rate, s_channels, s_application, &p_error);
    if (p_error < 0) {
      std::cout << "Failure::Encode:: Encoder could not be generated and "
                   "returned error: "
                << p_error << std::endl;
      return OPUSCODEC_FAILURE;
    }
    p_error = opus_encoder_ctl(p_encoder, OPUS_SET_BITRATE(p_bit_rate));
    if (p_error < 0) {
      std::cout << "Failure::Encode:: Encoder could not be configured with ctl "
                   "->error: "
                << p_error << std::endl;
      return OPUSCODEC_FAILURE;
    }
  }

  // Check if encoder storage (encoder_stream) exists
  if (encode_stream == nullptr) {
    // Create encoder_stream
    encode_stream = new (std::nothrow) unsigned char[s_max_packet_size];
    if (encode_stream == nullptr) {
      std::cout
          << "Failure::Encode:: unable to allocate space for encode_stream: "
          << std::endl;
      return OPUSCODEC_FAILURE;
    }
  }

  encode_stream_nb_bytes = opus_encode(p_encoder, input, s_frame_size,
                                       encode_stream, s_max_packet_size);
  if (encode_stream_nb_bytes < 0) {
    std::cout << "Failure::Encode:: encodingfailed with error: "
              << opus_strerror(encode_stream_nb_bytes) << std::endl;
    return OPUSCODEC_FAILURE;
  }

  return OPUSCODEC_SUCCESS;
}

opuscodec_status_t OpusCodec::Decode(unsigned char *input, int input_nb_samples) {
  if (verbose) {
    std::cout << "OpusCodec::Decode:: decode -> input_nb_samples: "
              << input_nb_samples << std::endl;
  }
  
  // Check if encoder exists
  if (p_decoder == nullptr) {
    // encoder does not yet exit -> generate encoder
    p_decoder = opus_decoder_create(s_sample_rate, s_channels, &p_error);
    if (p_error < 0) {
      std::cout << "Failure::Decode:: decoder could not be generated and "
                   "returned error: "
                << p_error << std::endl;
      return OPUSCODEC_FAILURE;
    }
  }

  // Check if encoder storage (encoder_stream) exists
  if (decode_stream == nullptr) {
    // Create encoder_stream
    decode_stream =
        new (std::nothrow) opus_int16[s_max_frame_size * s_channels];
    if (decode_stream == nullptr) {
      std::cout
          << "Failure::Decode:: unable to allocate space for decode_stream: "
          << std::endl;
      return OPUSCODEC_FAILURE;
    }
  }

  decode_stream_nb_samples =
      opus_decode(p_decoder, input, input_nb_samples, decode_stream,
                  s_max_frame_size, s_decode_fec);

  if (decode_stream_nb_samples < 0) {
    std::cout << "Failure::Decode:: decoding failed with error: "
              << opus_strerror(decode_stream_nb_samples) << std::endl;
    return OPUSCODEC_FAILURE;
  }

  return OPUSCODEC_SUCCESS;
}

OpusCodec::~OpusCodec() {
  if (p_encoder != nullptr) {
    opus_encoder_destroy(p_encoder);
    p_encoder = nullptr;
  }
  
  if (p_decoder != nullptr) {
    opus_decoder_destroy(p_decoder);
    p_decoder = nullptr;
  }
  
  std::cout << "OpusCodec::Destructor:: object destroyed " << std::endl;
}
