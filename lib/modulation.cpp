#include "modulation.hpp"

AMModem::AMModem(float m, float fc, liquid_ampmodem_type type, bool s) {
  if (fc > 0.5 || fc < -0.5) {
    throw std::invalid_argument("Frequency out of range");
  }
  mod = ampmodem_create(m, fc, type, (int)s);
}

AMModem::~AMModem() { ampmodem_destroy(mod); }

void AMModem::Print() { ampmodem_print(mod); }

void AMModem::Reset() { ampmodem_reset(mod); }

void AMModem::Modulate(float &x, std::complex<float> *y) {
  ampmodem_modulate(mod, x, (liquid_float_complex *)y);
}

void AMModem::Demodulate(std::complex<float> &y, float *x) {
  ampmodem_demodulate(mod, (liquid_float_complex)y, x);
}

void AMModem::Modulate(std::vector<float> &src,
                       std::vector<std::complex<float>> &dst) {
  ampmodem_modulate_block(mod, (float *)src.data(), (unsigned int)src.size(),
                          (liquid_float_complex *)dst.data());
}

void AMModem::Demodulate(std::vector<std::complex<float>> &src,
                         std::vector<float> &dst) {
  ampmodem_demodulate_block(mod, (liquid_float_complex *)src.data(),
                            (unsigned int)src.size(), (float *)dst.data());
}
