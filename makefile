CC=gcc
CXX=g++
RM=rm -f
TIDY=clang-tidy

INC_DIR=include
SRC_DIR=src
BIN_DIR=bin
LIB_DIR=lib
OBJ_DIR=build

LIQUID_DIR=liquid
OPUS_DIR=opus
FIFO_DIR=fifo

FLAGS=-Wall -I$(INC_DIR) -O3 -march=native
CFLAGS=$(FLAGS) -I. -fPIC -std=gnu11
CXXFLAGS=$(FLAGS) -std=c++11 -I$(LIQUID_DIR)/include -I$(OPUS_DIR)/include -I$(FIFO_DIR)/include
CXXLIBS=-lpthread $(LIQUID_DIR)/libliquid.a $(OPUS_DIR)/.libs/libopus.a

TIDYFLAGS=-checks=*,-clang-analyzer-alpha.*

SRCS=$(wildcard $(SRC_DIR)/*.cpp)
LIBS=$(wildcard $(LIB_DIR)/*.cpp)
INCS=$(wildcard $(INC_DIR)/*.hpp)
BINS=$(basename $(notdir $(SRCS)))
OBJS=$(addprefix $(OBJ_DIR)/,$(addsuffix .o,$(notdir $(basename $(LIBS)))))
OBIN=$(addprefix $(OBJ_DIR)/,$(addsuffix .o,$(BINS)))

# Build all binaries
programs: $(BINS)

# Build a binary
$(BINS): $(OBJS) $(OBIN)
	$(CXX) $(CXXFLAGS) $(OBJS) $(OBJ_DIR)/$@.o -o $(BIN_DIR)/$@ $(CXXLIBS)

# Build objects
$(OBJ_DIR)/%.o: $(LIB_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJ_DIR)/%.o: $(LIB_DIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

# Cleanup this project
clean:
	$(RM) $(OBJ_DIR)/* $(BIN_DIR)/*

tidy:
	$(TIDY) $(TIDYFLAGS) $(LIBS) $(SRCS) $(INCS) -- $(CXXFLAGS)

tidy-fix: TIDYFLAGS+= -fix
tidy-fix: tidy

# Build liquid and application
all: deps programs

clean-all: clean-deps clean

## Liquid and Opus stuff
deps: liquid opus

# Cleanup everything
clean-deps: liquid-clean opus-clean

# Configure liquid
.PHONY: configure
configure:
	cd $(LIQUID_DIR) && ./bootstrap.sh
	cd $(LIQUID_DIR) && CC="$(CC)" CFLAGS="$(CFLAGS)" ./configure
	cd $(OPUS_DIR) && ./autogen.sh
	cd $(OPUS_DIR) && CC="$(CC)" CFLAGS="$(CFLAGS)" ./configure

# Build liquid
.PHONY: liquid
liquid:
	export CC
	export CFLAGS
	make -e -C $(LIQUID_DIR)

# Build opus
.PHONY: opus
opus:
	export CC
	export CFLAGS
	make -e -C $(OPUS_DIR)

# Clean liquid
liquid-clean:
	make -C $(LIQUID_DIR) clean

# Clean opus
opus-clean:
	make -C $(OPUS_DIR) clean
