# SmoothSDR

For listening to that smooth sound of longwave radio.

## Installation

```sh
git submodule update --init --recursive
make configure
make all
```

## Pictures

![decode](https://git.snt.utwente.nl/rts2/sdr/-/jobs/artifacts/master/raw/graphics/decode.svg?job=decode)
![fastdecode](https://git.snt.utwente.nl/rts2/sdr/-/jobs/artifacts/master/raw/graphics/decode.svg?job=fastdecode)
