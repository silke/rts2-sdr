#pragma once

#include <iostream>
#include <new>
#include <opus.h>

enum opuscodec_status_t { OPUSCODEC_FAILURE, OPUSCODEC_SUCCESS };

class OpusCodec {
public:
  // Constructor -> sets variables for encoder and decoder
  // OpusCodec(int frame_size, int sample_rate, int channels, int bit_rate, int
  // max_frame_size, int max_packet_size, int application);
  // OpusCodec(int frame_size, int sample_rate, int channels, int bit_rate, int
  // max_frame_size, int max_packet_size, int
  // application=OPUS_APPLICATION_AUDIO);
  explicit OpusCodec(int frame_size = 960, int sample_rate = 48000, int channels = 2,
            int bit_rate = 64000, int max_frame_size = 6 * 960,
            int max_packet_size = 3 * 1276,
            int application = OPUS_APPLICATION_AUDIO);

  /*
  ENCODE
  desc: encodes input to opus stream
  req: - input is an array of size frame_size*channels
  returns: an int above 0 if operation was succesful
  */
  opuscodec_status_t Encode(opus_int16 *input, int input_nb_samples);
  opuscodec_status_t Decode(unsigned char *input, int input_nb_samples);

  // Destructor
  virtual ~OpusCodec();

  // public encoder and decoder variables
  unsigned char *encode_stream{nullptr};
  int encode_stream_nb_bytes{0};

  opus_int16 *decode_stream{nullptr};
  int decode_stream_nb_samples{0};

  bool verbose{false};

private:
  // opus variables
  int s_frame_size{0};
  int s_sample_rate{0};
  int s_channels{0};
  int s_max_frame_size{0};
  int s_max_packet_size{0};
  int s_application{0};   // ~Something so that opus knows what type of data its
                       // processing
  int s_decode_fec{0}; // flag for some error correction

  int p_bit_rate{0}; // Sets bitrate of encoded input (-> the opus stream)
  int p_error{0};

  OpusEncoder *p_encoder{nullptr};
  OpusDecoder *p_decoder{nullptr};
};
