#pragma once

#include <complex>
#include <liquid.h>
#include <vector>

class Resampler {
private:
  resamp_rrrf q;

public:
  Resampler(float rate);
  Resampler(float rate, unsigned int m, float fc, float As, unsigned int npfb);
  ~Resampler();

  void Print();
  void Reset();
  unsigned int GetDelay();
  void SetRate(float rate);
  void AdjustRate(float delta);

  void Execute(float &x, std::vector<float> &y);
  void Execute(std::vector<float> &x, std::vector<float> &y);
};
