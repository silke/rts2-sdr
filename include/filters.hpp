#pragma once

#include <complex>
#include <liquid.h>
#include <vector>

#define FILTER_ORDER 5
#define FILTER_STOP 60.0
#define FILTER_RIPPLE 1.0

class IIRFilter {
public:
  iirfilt_crcf q;

  IIRFilter(float f_carrier, float f_width, float f_sample);
  ~IIRFilter();

  void Print();
  void Reset();
  void Execute(std::complex<float> &x, std::complex<float> *y);
  void Execute(std::vector<std::complex<float>> &x,
               std::vector<std::complex<float>> &y);
  unsigned int GetLength();
  void FrequencyResponse(float fc, std::complex<float> *H);
  float GroupDelay(float fc);
};