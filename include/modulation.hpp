#pragma once

#include <complex>
#include <liquid.h>
#include <stdexcept>
#include <vector>

class AMModem {
private:
  ampmodem mod;

public:
  /*
   * Create AMModem
   * m    : modulation index
   * fc   : carrier frequency, range: [-0.5, 0,5]
   * type : AM type
   * s    : Suppressed carrier (or not)
   */
  AMModem(float m, float fc, liquid_ampmodem_type type, bool s);
  ~AMModem();

  void Print();
  void Reset();
  void Modulate(float &x, std::complex<float> *y);
  void Modulate(std::vector<float> &src, std::vector<std::complex<float>> &dst);
  void Demodulate(std::complex<float> &y, float *x);
  void Demodulate(std::vector<std::complex<float>> &src,
                  std::vector<float> &dst);
};
