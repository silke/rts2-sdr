#pragma once

#include <complex>
#include <vector>

void PlotSignals(std::string file, const std::vector<float> &x,
                 const std::vector<std::complex<float>> &y,
                 const std::vector<float> &z, float f_sample);

void PlotSignal(std::string file, const std::vector<float> &x, float f_sample);
void PlotSignal(std::string file, const std::vector<std::complex<float>> &x,
                float f_sample);

void AddSignals(std::vector<float> &a, std::vector<float> &b);
void AddSignals(std::vector<float> &a, std::vector<float> &b,
                std::vector<float> &c);
void AddSignals(std::vector<std::complex<float>> &a,
                std::vector<std::complex<float>> &b);
void AddSignals(std::vector<std::complex<float>> &a,
                std::vector<std::complex<float>> &b,
                std::vector<std::complex<float>> &c);

void GenerateSineWave(std::vector<float> &s, float sr, float f, float a);
