#include <chrono>
#include <complex>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <queue>
#include <thread>
#include <vector>

#include "atomicops.h"
#include "readerwriterqueue.h"

#include "filters.hpp"
#include "modulation.hpp"
#include "resampler.hpp"
#include "support.hpp"

#define AM_TYPE LIQUID_AMPMODEM_DSB
#define AM_SUPR 0
#define AM_SPACING 9e3f
#define AM_SAMPLE 576e3f

#define AM_CHANNELS 14
#define AM_START 153e3f
#define AM_INDEX 0.1

#define AUDIO_SAMPLE 96e3f
#define AUDIO_RATIO (unsigned long)(AM_SAMPLE / AUDIO_SAMPLE)

typedef std::chrono::steady_clock Time;

// Create the simulation samples
void GenerateSamples(float f_signal,
                     std::vector<std::complex<float>> &samples) {
  float fc;
  std::vector<float> x(samples.size());
  std::vector<std::complex<float>> y(samples.size());

  for (int i = 0; i < AM_CHANNELS; i++) {
    fc = (AM_START + i * AM_SPACING) / AM_SAMPLE;

    // Generate signal
    GenerateSineWave(x, AM_SAMPLE, f_signal + 100 * i, 1);

    // Modulate signal
    AMModem mod(AM_INDEX, fc, AM_TYPE, AM_SUPR);
    mod.Modulate(x, y);

    // Add to other signals
    AddSignals(samples, y);
  }
}

void SourceThread(
    std::vector<std::complex<float>> samples,
    moodycamel::BlockingReaderWriterQueue<std::complex<float>> *dst) {
  // Push samples to output queue
  for (auto const s : samples) {
    dst->enqueue(s);
  }
}

void FilterThread(
    int samples, float f_carrier,
    moodycamel::BlockingReaderWriterQueue<std::complex<float>> *src,
    moodycamel::BlockingReaderWriterQueue<std::complex<float>> *dst) {
  std::complex<float> x, y;

  // Create filter
  IIRFilter filter(f_carrier, AM_SPACING, AM_SAMPLE);

  for (int i = 0; i < samples; i++) {
    // Get sample from input
    src->wait_dequeue(x);

    // Filter sample
    filter.Execute(x, &y);

    // Push to output
    dst->enqueue(y);
  }
}

void DemodulatorThread(
    int samples, float f_carrier,
    moodycamel::BlockingReaderWriterQueue<std::complex<float>> *src,
    moodycamel::BlockingReaderWriterQueue<float> *dst) {
  std::complex<float> x;
  float y;

  // Create modem
  AMModem am(AM_INDEX, f_carrier / AM_SAMPLE, AM_TYPE, AM_SUPR);

  for (int i = 0; i < samples; i++) {
    // Get sample from input
    src->wait_dequeue(x);

    // Demodulate sample
    am.Demodulate(x, &y);

    // Push to output
    dst->enqueue(y);
  }
}

void ResampleThread(int audioSamples,
                    moodycamel::BlockingReaderWriterQueue<float> *src,
                    moodycamel::BlockingReaderWriterQueue<float> *dst) {
  Resampler r(AUDIO_SAMPLE / AM_SAMPLE);
  std::vector<float> buffer(AUDIO_RATIO);
  std::vector<float> audio(1);

  for (int i = 0; i < audioSamples; i++) {
    // Get samples from input
    for (auto &x : buffer) {
      src->wait_dequeue(x);
    }

    // Resample samples
    r.Execute(buffer, audio);

    // Push to output
    dst->enqueue(audio[0]);
  }
}

void StoreThread(moodycamel::BlockingReaderWriterQueue<float> *src,
                 std::vector<float> *dst) {
  for (auto &y : *dst) {
    src->wait_dequeue(y);
  }
}

int main(int argc, char *argv[]) {
  moodycamel::BlockingReaderWriterQueue<std::complex<float>> inputSamples;
  moodycamel::BlockingReaderWriterQueue<std::complex<float>> filteredSamples;
  moodycamel::BlockingReaderWriterQueue<float> demodSamples;
  moodycamel::BlockingReaderWriterQueue<float> audioSamples;

  // Usage
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " channel time " << std::endl;
    std::cerr << std::setw(10) << "channel: ";
    std::cerr << "AM channel from 0 to 13" << std::endl;
    std::cerr << std::setw(10) << "time: ";
    std::cerr << "time to plot in seconds" << std::endl;
    return 1;
  }

  // Frequency of signal in Hz
  float fSignal = 1e3;
  unsigned long signal =
      (unsigned long)atoi(argv[1]); // Channel selection (0 indexed)
  float time = (float)atof(argv[2]);
  float fCarrier = (float)signal * AM_SPACING + AM_START;

  // Number of inputSamples for test
  unsigned long sampleCount = (unsigned long)(time * AM_SAMPLE);

  // Some information
  std::cout << "Carrier frequency: " << fCarrier / 1e3 << " kHz" << std::endl
            << "Signal frequency: " << (fSignal + 1e2 * signal) / 1e3 << " kHz"
            << std::endl
            << "Resampling rate: " << AUDIO_RATIO << std::endl;

  // Generate input signal
  std::complex<float> z(0.0, 0.0);
  std::vector<float> outputSignal(sampleCount / AUDIO_RATIO);
  std::vector<std::complex<float>> inputSignal(sampleCount, z);
  GenerateSamples(fSignal, inputSignal);

  // Get start time
  auto begin = Time::now();

  // Start processing in threads
  std::thread source(SourceThread, inputSignal, &inputSamples);
  std::thread filter(FilterThread, (int)sampleCount, fCarrier, &inputSamples,
                     &filteredSamples);
  std::thread demodulator(DemodulatorThread, (int)sampleCount, fCarrier,
                          &filteredSamples, &demodSamples);
  std::thread resample(ResampleThread, (int)sampleCount / AUDIO_RATIO,
                       &demodSamples, &audioSamples);
  std::thread store(StoreThread, &audioSamples, &outputSignal);

  source.join();
  filter.join();
  demodulator.join();
  resample.join();
  store.join();

  // Get stop timexx
  auto end = Time::now();
  std::chrono::duration<float> duration = (end - begin);

  std::cout << "Processed " << sampleCount << " samples in "
            << duration.count() * 1e3f << " ms ("
            << (double)sampleCount / duration.count() / 1e3f << " ksamples/s)"
            << std::endl;

  // Plot signal
  std::vector<float> x(sampleCount / AUDIO_RATIO);
  GenerateSineWave(x, AUDIO_SAMPLE, (float)(fSignal + 1e2 * signal), 1.0);
  PlotSignal("data/signal_audio_in.dat", x, AUDIO_SAMPLE);
  PlotSignal("data/signal_radio.dat", inputSignal, AM_SAMPLE);
  PlotSignal("data/signal_audio_out.dat", outputSignal, AUDIO_SAMPLE);

  return 0;
}
