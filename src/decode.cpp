#include <complex>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <vector>

#include "filters.hpp"
#include "modulation.hpp"
#include "support.hpp"

#define AM_TYPE LIQUID_AMPMODEM_DSB
#define AM_SUPR 0
#define AM_SPACING 9e3f

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " channel time " << std::endl;
    std::cerr << std::setw(10) << "channel: ";
    std::cerr << "AM channel from 0 to 13" << std::endl;
    std::cerr << std::setw(10) << "time: ";
    std::cerr << "time to plot in seconds" << std::endl;
    return 1;
  }

  unsigned long signals = 14; // Number of channels
  unsigned long signal =
      (unsigned long)atoi(argv[1]); // Channel selection (0 indexed)
  float time = (float)atof(argv[2]);

  float f_signal = 1e3;    // Frequency of signal in Hz
  float f_sample = 600e3;  // Sample rate in Hz
  float f_carrier = 153e3; // Carrier frequency
  float mod_index = 0.1;   // Modulation index

  // Number of samples for test
  unsigned long samples = (unsigned long)(time * f_sample);

  // Frequency index
  std::vector<float> fc(signals);

  // Output signal
  std::complex<float> z(0.0, 0.0);
  std::vector<float> s;
  std::vector<std::complex<float>> a(samples, z);

  // Create carrier frequencies etc
  for (unsigned long i = 0; i < signals; i++) {
    fc[i] = (f_carrier + i * AM_SPACING) / f_sample;

    // Generate signal
    float f = f_signal + 100 * i;
    std::vector<float> x(samples);
    GenerateSineWave(x, f_sample, f, 1);

    // Modulate signal
    std::vector<std::complex<float>> y(samples);
    AMModem mod(mod_index, fc[i], AM_TYPE, AM_SUPR);
    mod.Modulate(x, y);

    if (i == signal) {
      s = x;
    }

    // Add to other signals
    AddSignals(a, y);
  }

  // Demodulated & filtered signals
  std::vector<float> d(samples);
  std::vector<std::complex<float>> o(samples);

  // IIRFilter signals
  IIRFilter f(fc[signal] * f_sample, AM_SPACING, f_sample);
  f.Execute(a, o);

  // Demodulate!
  AMModem demod(mod_index, fc[signal], AM_TYPE, AM_SUPR);
  demod.Demodulate(o, d);

  // Some information
  std::cout << "Carrier frequency: " << fc[signal] * f_sample / 1000.0 << " kHz"
            << std::endl
            << "Signal frequency: " << (f_signal + 100 * signal) / 1000.0
            << " kHz" << std::endl;

  // Plot the signal
  PlotSignal("data/signal_audio_in.dat", s, f_sample);
  PlotSignal("data/signal_radio.dat", o, f_sample);
  PlotSignal("data/signal_audio_out.dat", d, f_sample);

  return 0;
}
