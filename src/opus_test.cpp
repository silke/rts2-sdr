#include "opuscodec.hpp"
#include <iostream>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/*Hardcoded settings*/
#define FRAME_SIZE 960 //sampling_rate/number -> //Lower is better but higher could be better at lower bitrates
#define SAMPLE_RATE 48000 //"Supported sampling rates are 8000, 12000, "16000, 24000 and 48000.
#define CHANNELS 2
#define APPLICATION OPUS_APPLICATION_AUDIO
#define BITRATE 64000
#define MAX_FRAME_SIZE 6*960
#define MAX_PACKET_SIZE (3*1276)

#define FAILURE 0

struct WAVE_HEADER{
    char Chunk[4];
    int ChunkSize;
    char format[4];
    char Sub_chunk1ID[4];
    int Sub_chunk1Size;
    short int AudioFormat;
    short int NumChannels;
    int SampleRate;
    int ByteRate;
    short int BlockAlign;
    short int BitsPerSample;
    char Sub_chunk2ID[4];
    int Sub_chunk2Size;
};

struct WAVE_HEADER waveheader;

char * inFile; //[] = "guitar.wav";
char * outFile; //[] = "output.wav";

int main(int argc, char ** argv) {
  std::cout << "usermsg:: Running OpusCodec Main/Test" << std::endl;
  
  if(argc != 3) {
    std::cout << "usermsg:: Invalid number of arguments:" << argc << " -> supply input and output" << std::endl;
    return EXIT_FAILURE;
  }
  
  inFile = argv[1];
  outFile = argv[2];
  
  FILE * fin = fopen(inFile, "r");
  FILE * fout = fopen(outFile, "w");
  
  if(!fin || !fout) return EXIT_FAILURE;
  
  OpusCodec opus = OpusCodec(FRAME_SIZE, SAMPLE_RATE, CHANNELS, BITRATE, MAX_FRAME_SIZE, MAX_PACKET_SIZE);
  opus.verbose = true;
  
  opus_int16 in[FRAME_SIZE*CHANNELS];
  opus_int16 out[FRAME_SIZE*CHANNELS];
  
  fread(&waveheader, sizeof(waveheader), 1, fin);
  fwrite(&waveheader, sizeof(waveheader), 1, fout);
  
  while(true) {
    int i;
  
    //unsigned char pcm_bytes[MAX_FRAME_SIZE*CHANNELS*2];// Used for reordering
        
    int in_nb_samples = fread(in, sizeof(opus_int16)*CHANNELS, FRAME_SIZE, fin);
    // Used for reordering
    /* Convert from little-endian ordering. */
    //for (i=0;i<CHANNELS*FRAME_SIZE;i++)
    //  in[i]=pcm_bytes[2*i+1]<<8|pcm_bytes[2*i];
    
    if(!(in_nb_samples == FRAME_SIZE)) {
      std::cout << "MAIN:: nb samples is not frame_size " << std::endl;
      //break;
    }
    if(feof(fin) || ferror(fin)) {
      std::cout << "MAIN:: end of file " << std::endl;
      break;
    }

    if(opus.Encode(in, in_nb_samples) == OPUSCODEC_FAILURE) break; //stop when failure is detected
    if(opus.Decode(opus.encode_stream, opus.encode_stream_nb_bytes) == OPUSCODEC_FAILURE) break; 
    
    /* Convert to little-endian ordering. */
    //for(i=0;i<CHANNELS*FRAME_SIZE;i++) {
    //  pcm_bytes[2*i]=opus.decode_stream[i]&0xFF;
    //  pcm_bytes[2*i+1]=(opus.decode_stream[i]>>8)&0xFF;
    //}
    
    int elements_written = fwrite(opus.decode_stream, sizeof(opus_int16), opus.decode_stream_nb_samples*CHANNELS, fout); //write output to file
    std::cout << "MAIN:: elements written " << elements_written << std::endl;
    
  }
  
  opus.~OpusCodec();
  fclose(fin);
  fclose(fout);
  return EXIT_SUCCESS;
  
}
