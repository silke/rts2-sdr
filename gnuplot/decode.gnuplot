set term svg enhanced fname 'sans-serif' size 800,400
set output "graphics/decode.svg"

reset                                   # reset
set size ratio 0.2                      # set relative size of plots
set xlabel 'time (s)'                   # set x-axis label for all plots
set grid xtics ytics                    # grid: enable both x and y lines
set grid lt 1 lc rgb '#cccccc' lw 1     # grid: thin gray lines
set multiplot layout 2,1 scale 1.0,1.0  # set three plots for this figure

# real
set ylabel 'real'                       # set y-axis label
set yrange [-1.2:1.2]                   # set plot range
plot 'data/signal_radio.dat'     using 2:3 with lines lt 1 lw 2 notitle ,\
     'data/signal_audio_in.dat'  using 2:3 with lines lt 2 lw 2 notitle ,\
     'data/signal_audio_out.dat' using 2:3 with lines lt 3 lw 2 notitle

# imag
set ylabel 'imag'                       # set y-axis label
set yrange [-1.2:1.2]                   # set plot range
plot 'data/signal_radio.dat' using 2:4 with lines lt 1 lw 2 lc rgb '#004080' notitle


unset multiplot
